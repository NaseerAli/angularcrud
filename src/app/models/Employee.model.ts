export class Employee{
    id:string;
    username: string;
    password: string;
    ConfPassword: string;
    Email: string;
    FullName: string;
    Mobile: string;
    Country: string;
    City: string;
    Address: string;
    Zipcode: string;
}