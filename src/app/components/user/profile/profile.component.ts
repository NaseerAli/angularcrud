import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { error } from 'protractor';
import { Employee } from 'src/app/models/Employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  ArrEmployee:  Employee[];

  constructor(
    private empService:EmployeeService,
    public router:Router
  ) { }

  ngOnInit(): void {
    debugger
    this.GetEmployee();
  }

  GetEmployee()
  {
    this.empService.GetEmployee().subscribe
    (
      (res: any) =>
      {
        if(res.retCode == 1)
        {
          this.ArrEmployee = res.ListUser
        }
      },
      (error) =>
      {
        console.error(error.message)
      }
    );
  }

  Delete(emp:Employee)
  {
    if(confirm("Are you sure want to delete "+ emp.FullName+"?"))
    {      
      this.empService.DeleteUser(emp).subscribe(
        (res:any) =>
        {
          if(res.retCode == 1)
          {
            alert(emp.FullName+" is deleted.");
            this.GetEmployee();
          }
        },
        (error) =>
        {
          console.error(error.message);
        }
      )
    }
  }

  Update(emp:Employee)
  {
    localStorage.setItem(emp.id, JSON.stringify(emp));
    this.router.navigate(["/update"], { queryParams: {id: emp.id}});
  }

}
