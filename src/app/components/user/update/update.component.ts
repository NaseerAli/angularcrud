import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { error } from 'protractor';
import { Employee } from 'src/app/models/Employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  constructor(private empService:EmployeeService, public router:ActivatedRoute, public r1:Router ) { }
  arrEmployee:Employee = new Employee();

  ngOnInit(): void {
    var ID = this.router.snapshot.queryParamMap.get("id");
    this.arrEmployee = JSON.parse(localStorage.getItem(ID));
  }

  update()
  {
    this.empService.Update(this.arrEmployee).subscribe(
      (res:any)=>
      {
        if (res.retCode == 1)
        {
          alert(this.arrEmployee.FullName + " is updated successfully.");
          this.r1.navigate(["profile"]);
        }
      },
      (error)=>
      {
        console.error(error.message);
      }
    )
  }

}
