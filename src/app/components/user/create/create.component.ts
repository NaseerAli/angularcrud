import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/Employee.model';
import { EmployeeService } from 'src/app/services/employee.service';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  arrEmployee:Employee = new Employee();

  
  constructor(private employeeService:EmployeeService) { }

  ngOnInit(): void {


  }

  RegisterUser()
  {

      this.employeeService.UserRegisteration(this.arrEmployee).subscribe(
        (res:any)=>
        {
          if(res.retCode == 1)
          {
            alert("Congrates!! " +this.arrEmployee.FullName +" is registered.");
          }
        },
        (error)=>
        {
          alert("Opps!! Something went wrong. Please try again.");
        }
      );
  }

  MustMatch(controlName: string, matchingControlName: string):boolean {
        
        // set error on matchingControl if validation fails
        if (controlName !== matchingControlName) {
            return true;
        } else {
            return false;
        }
    }
}


