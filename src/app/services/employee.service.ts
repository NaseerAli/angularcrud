import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from '../models/Employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private httpclient:HttpClient) { }

  GetEmployee()
  {
    return this.httpclient.post( 'http://localhost:54611/WebServices/UserHandler.asmx/JsonGetAllUserData',
    {},
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  } 

  UserRegisteration(emp:Employee)
  {
    return this.httpclient.post( 'http://localhost:54611/WebServices/UserHandler.asmx/JSonRegisterUser',
    {arrUser:emp},
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  DeleteUser(emp:Employee)
  {
    return this.httpclient.post('http://localhost:54611/WebServices/UserHandler.asmx/JSonDelete',
    {arrUser:emp},
    { headers: new HttpHeaders({ 'Content-Type' : 'application/json'}) });
  }

  Update(user:Employee)
  {
    return this.httpclient.post('http://localhost:54611/WebServices/UserHandler.asmx/JSonUpdate',
    {user},
    { headers: new HttpHeaders({ 'Content-type' : 'application/json'})});
  }
}
