import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './components/user/create/create.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { UpdateComponent } from './components/user/update/update.component';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    data:{
      title:'',
      description:''
    },
  },
  {
    path: 'create',
    component: CreateComponent,
    data:{
      title:'',
      description:''
    },
  },
  {
    path:'update',
    component:UpdateComponent,
    data:{
      title:'',
      description:''
    },
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
